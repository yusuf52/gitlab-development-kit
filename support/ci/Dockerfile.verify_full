ARG from_image
FROM ${from_image}

LABEL authors.maintainer "GDK contributors: https://gitlab.com/gitlab-org/gitlab-development-kit/-/graphs/main"

ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

## We are building this docker file with an experimental --squash in order
## to reduce the resulting layer size: https://docs.docker.com/engine/reference/commandline/build/#squash-an-images-layers---squash-experimental
##
## The CI script that build this file can be found under: support/docker
ARG URL
ARG SHA

ARG GITLAB_CI_CACHE_DIR
ARG GDK_INTERNAL_CACHE_FULL_DIR
ARG BUNDLE_PATH
ARG GEM_HOME
ARG GEM_PATH
ARG GOCACHE
ARG GOMODCACHE
ARG NODE_PATH
ARG PUMA_SINGLE_MODE
ARG GDK_DEBUG

RUN env | grep -Ev "TOKEN|PASSWORD|LICENSE|KEY"

RUN sudo mkdir -p ${BUNDLE_PATH} ${GEM_HOME} ${GEM_PATH} ${GOCACHE} ${GOMODCACHE} ${NODE_PATH}
RUN sudo chown -R gdk:gdk ${GDK_INTERNAL_CACHE_FULL_DIR} || true

WORKDIR /home/gdk

COPY --chown=gdk ${GITLAB_CI_CACHE_DIR}/ ${GITLAB_CI_CACHE_DIR}/

RUN du -smx ${GITLAB_CI_CACHE_DIR}/* || true

RUN echo "yarn-offline-mirror ${NODE_PATH}/.yarn-cache/" >> ${HOME}/.yarnrc
RUN echo "yarn-offline-mirror-pruning true" >> ${HOME}/.yarnrc

RUN curl --fail "${URL}/-/raw/${SHA}/support/install" | bash -s - gdk "${SHA}" && \
  (cd gdk && GDK_KILL_CONFIRM=true gdk kill)

WORKDIR /home/gdk/gdk

RUN gdk config set gitlab.cache_classes true && gdk config set webpack.live_reload false && gdk config set webpack.sourcemaps false && make Procfile
